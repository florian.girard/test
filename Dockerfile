From python:3

COPY . /app
WORKDIR /app

RUN pip install -r requirements.txt

EXPOSE 8001

ENTRYPOINT ["python","app.py"]
